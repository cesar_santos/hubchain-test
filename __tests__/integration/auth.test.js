const request = require("supertest");
const app = require("../../src/app");

describe("Auth", () => {
  it("Should register", async () => {
    const response = await request(app)
      .post("/register")
      .send({
        name: "Teste",
        email: "teste@teste.com",
        password: "123456"
      });
    expect(response.statusCode).toBe(201);
  });

  it("Should log in", async () => {
    const response = await request(app)
      .post("/auth")
      .send({
        email: "teste@teste.com",
        password: "123456"
      });
    expect(response.statusCode).toBe(201);
  });
});
