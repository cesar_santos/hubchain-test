const request = require("supertest");
const app = require("../../src/app");

const token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJ0ZXN0ZUB0ZXN0ZSxjb20iLCJpYXQiOjE1ODI5MDMxMDYsImV4cCI6MTU4Mjk4OTUwNn0.g59NPjCeihDb7mep9yezS-4n3qZh5JO0llrNt1k8aoM`;

describe("Products", () => {
  it("It should response the POST/ method", async () => {
    const response = await request(app)
      .post("/api/v1/products")
      .set("Authorization", "bearer " + token)
      .send({
        name: "Casa 1",
        description: "Casa Nova",
        price: 400000
      });
    expect(response.statusCode).toBe(201);
  });

  it("It should response the PUT/id method", async () => {
    const response = await request(app)
      .put("/api/v1/products/1")
      .set("Authorization", "bearer " + token)
      .send({
        name: "Casa Alterada",
        description: "Casa Velha",
        price: 500000
      });
    expect(response.statusCode).toBe(200);
  });

  it("It should response the GET method", async () => {
    const response = await request(app)
      .get("/api/v1/products")
      .set("Authorization", "bearer " + token);
    expect(response.statusCode).toBe(200);
  });

  it("It should response the GET/id method", async () => {
    const response = await request(app)
      .get("/api/v1/products/1")
      .set("Authorization", "bearer " + token);
    expect(response.statusCode).toBe(200);
  });

  it("It should response the DELETE/id method", async () => {
    const response = await request(app)
      .delete("/api/v1/products/1")
      .set("Authorization", "bearer " + token);
    expect(response.statusCode).toBe(204);
  });
});
