const request = require("supertest");
const app = require("../../src/app");

const token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJ0ZXN0ZUB0ZXN0ZSxjb20iLCJpYXQiOjE1ODI5MDMxMDYsImV4cCI6MTU4Mjk4OTUwNn0.g59NPjCeihDb7mep9yezS-4n3qZh5JO0llrNt1k8aoM`;

describe("User", () => {
  it("Should return user", async () => {
    const response = await request(app)
      .get("/api/v1/users")
      .set("Authorization", "bearer " + token);
    expect(response.statusCode).toBe(200);
  });

  it("Should change user", async () => {
    const response = await request(app)
      .put("/api/v1/users")
      .set("Authorization", "bearer " + token)
      .send({
        name: "Teste Alterado",
        email: "teste@teste.com",
        password: "123456"
      });
    expect(response.statusCode).toBe(200);
  });

  it("Should delete user", async () => {
    const response = await request(app)
      .delete("/api/v1/users")
      .set("Authorization", "bearer " + token);
    expect(response.statusCode).toBe(204);
  });
});
