const express = require("express");
const router = express.Router();

const ProductController = require("../app/controllers/ProductController");
const UserController = require("../app/controllers/UserController");

router.get("/users", UserController.all);
// router.get("/users/:id", UserController.byId);
// router.post("/users", UserController.create);
router.put("/users", UserController.update);
router.delete("/users", UserController.destroy);

router.get("/products", ProductController.all);
router.get("/products/:id", ProductController.byId);
router.post("/products", ProductController.create);
router.put("/products/:id", ProductController.update);
router.delete("/products/:id", ProductController.destroy);

module.exports = router;
