module.exports = {
  dialect: "mysql",
  host: "localhost",
  username: "root",
  password: "123",
  database: "hubchain",
  define: {
    timestamps: true,
    underscored: true,
    underscoredAll: true
  }
};
