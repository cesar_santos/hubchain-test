const express = require("express");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const app = express();

const config = require("./config/secret");
const routes = require("./routes/routes");
const auth = require("./routes/auth");

const validateToken = (req, res, next) => {
  const hash = req.headers.authorization;

  if (!hash) {
    return res.status(401).json({
      message: "Unauthorized access"
    });
  }
  const token = hash.split(" ");

  if (!token.length === 2) {
    return res.status(401).json({
      message: "Unauthorized access"
    });
  }

  jwt.verify(token[1], config.secret, (error, decoded) => {
    if (error) {
      return res.status(401).json({
        message: "Token invalid"
      });
    }
    req.userId = decoded.id;
    req.email = decoded.email;

    next();
  });
};

app.use(bodyParser.json());
app.use(auth);
app.use("/api/v1", validateToken, routes);

module.exports = app;
