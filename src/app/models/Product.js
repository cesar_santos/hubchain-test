module.exports = (sequelize, Datatype) => {
  return sequelize.define("Product", {
    name: Datatype.STRING,
    description: Datatype.STRING,
    price: Datatype.FLOAT
  });
};
