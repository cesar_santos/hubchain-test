const bcryptjs = require("bcryptjs");

module.exports = (sequelize, Datatype) => {
  const User = sequelize.define(
    "User",
    {
      name: Datatype.STRING,
      email: Datatype.STRING,
      password: Datatype.VIRTUAL,
      password_hash: Datatype.STRING
    },
    {
      hooks: {
        beforeSave: async user => {
          if (user.password) {
            user.password_hash = await bcryptjs.hash(user.password, 10);
          }
        }
      }
    }
  );

  return User;
};
