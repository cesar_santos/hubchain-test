const { User } = require("../models");
const bcryptjs = require("bcryptjs");
const createToken = require("../../helpers/create-token");
module.exports = {
  register: async (req, res) => {
    try {
      const user = await User.create(req.body);
      const token = await createToken(user.id, user.email);

      user.password = undefined;
      user.password_hash = undefined;

      res.status(201).json({
        user,
        token
      });
    } catch (error) {
      console.error(error.name);
      if (error.name === "SequelizeUniqueConstraintError") {
        res.status(400).json({
          message: "Erro, este email já cadastrado"
        });
      }
      res.status(500).json({
        message: "Erro ao cadastrar novo usuário"
      });
    }
  },
  auth: async (req, res) => {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ where: { email } });
      if (!user) {
        return res.status(400).json({
          message: "Usuário não encontrado"
        });
      }
      if (!(await bcryptjs.compare(password, user.password_hash))) {
        return res.status(400).json({
          message: "Senha inválida"
        });
      }
      const token = await createToken({ id: user.id, email: user.email });
      return res.status(201).json({
        token
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao autenticar login e senha"
      });
    }
  }
};
