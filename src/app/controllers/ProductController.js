const { Product } = require("../models");
module.exports = {
  all: async (req, res) => {
    try {
      const products = await Product.findAll();
      res.json({
        products
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao lista produtos"
      });
    }
  },
  byId: async (req, res) => {
    try {
      const product = await Product.findByPk(req.params.id);
      res.json({
        product
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao exibir produto"
      });
    }
  },
  create: async (req, res) => {
    try {
      const product = await Product.create(req.body);
      return res.status(201).json({
        product
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao criar produto"
      });
    }
  },
  update: async (req, res) => {
    try {
      await Product.update(req.body, {
        where: {
          id: req.params.id
        }
      });
      res.json({
        product: req.body
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao atulizar o produto"
      });
    }
  },
  destroy: async (req, res) => {
    try {
      await Product.destroy({
        where: {
          id: req.params.id
        }
      });
      res.status(204).json({});
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao deleter o produto"
      });
    }
  }
};
