const { User } = require("../models");
module.exports = {
  all: async (req, res) => {
    try {
      const user = await User.findByPk(req.userId);
      res.json({
        user
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao retornar dados do usuário"
      });
    }
  },
  // byId: async (req, res) => {},
  // create: async (req, res) => {},
  update: async (req, res) => {
    try {
      const user = User.update(req.body, {
        where: {
          id: req.userId
        }
      });

      res.json({
        user
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao atualizada dados do usuário"
      });
    }
  },
  destroy: async (req, res) => {
    try {
      await User.destroy({
        where: {
          id: req.userId
        }
      });
      res.status(204).json({});
    } catch (error) {
      console.error(error);
      res.status(500).json({
        message: "Erro ao deletar usuário"
      });
    }
  }
};
