const jwt = require("jsonwebtoken");
const config = require("../config/secret");

const createToken = async (id, email) => {
  const token = await jwt.sign({ id, email }, config.secret, {
    expiresIn: 86400
  });

  return token;
};

module.exports = createToken;
