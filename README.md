# Teste Hubchain

- Na raiz do projeto instale todas as dependências do projeto com o comando npm i
- Depois crie um banco com o nome de hubchain
- Agora para criar as tabelas precisamos executar as migrations no terminal execute o seguinte comando npx db:migrate
- Agora é preciso iniciar o servidor da aplicação, no terminal execute o seguinte comando npm start
- O servidor sera iniciado por padrão na porta 3000
- Se por algum motivo a porta 3000 estiver ocupada execute o seguinte comando PORT=3010 npm start

## Rotas

- Criar Registro POST/register
- Corpo da requisição

```js

	{
		"name": "Teste",
		"email": "teste@teste.com",
		"password": "123456"
	}

```

- Efetuar Log in POST/auth
- Corpo da requisição

```js
  {
	"email": "teste@teste.com",
	"password": "123456"
}
```

## Rotas privadas

### Users

- Obter dados do usuário logado GET/api/v1/users

- Altera dados do usuário logado PUT/api/v1/users
- Corpo da Requisição

```js
{
	"name": "Teste",
	"email": "teste@teste.com",
	"password": "123456"
}

```

- Deletar usuário logado DELETE/api/v1/users

### Products

- Obter lista de produtos GET/api/v1/products
- Obter protuto especifico GET/api/v1/products/id
- Criar novo produto POST/api/v1/products
- Corpo da requisição

```js

{
	"name": "Produto Teste",
	"description": "Descrição do Produto Teste",
	"price": 400000
}

```

- Alterar produtos PUT/api/v1/products/id
- Corpo da requisição

```js

{
	"name": "Produto Teste",
	"description": "Descrição do Produto Teste",
	"price": 400000
}
```

- Deletar produto DELETE/api/v1/products/id
